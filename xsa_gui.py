#!/usr/bin/env python3

if __name__ == "__main__":

    from argparse import ArgumentParser
    parser = ArgumentParser(description='search and download gui')
    parser.add_argument('--host', type=str, help='hostname or ip-address (including port).', default="http://localhost:8080")
    parser.add_argument('--user', type=str, help='user:passw', default=":")

    args = parser.parse_args()

    from xsaweb.main import start_web_gui
    start_web_gui(args)
