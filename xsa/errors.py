"""
:Author: Franziska Koehn
:Created: 2015/01/13

This module houses all classes of Exceptions of xsa.
Some are defined here just to hide exceptions from different libraries.
"""

class Error(Exception):
    """A new Exception-class, to differentiate between xsa and non-xsa Exceptions"""
    pass

class ServerNotFoundError(Error):
    """Should be raised when the server was not found."""
    pass

class DatabaseError(Error):
    """Should be raised when a DatabaseError occured."""
    pass

class UnauthorizedError(Error):
    """Should be raised when the user-name or -password was incorrect."""
    pass

class ResponseNotReady(Error):
    """Should be raised when the response was not ready."""
    pass

class EmptyResultset(Error):
    """Should be raised when the resultset of the given search is empty."""
    pass

class QueryError(Error):
    """Should be raised when the query-definition is incompatible."""
    pass

class NoRestApiError(Error):
    """Should be raised when no REST-API was defined in the json-file of a root-type"""
    pass

class NoExtraSourceError(Error):
    """Should be raised when no comparision was defined in the json-file of a root-type"""
    pass

class CorruptedQueryError(Error):
    """Should be raised when the query-definition can not be loaded from file."""
    pass

class DownloadError(Error):
    """Should be raised when an error occurred while downloading."""
    pass

class WritingError(Error):
    """Should be raised when an error occurred while writing something."""
    pass
