"""

:Author: Franziska Koehn
:Created: 2015/01/13

This module contains functions for reading the datatypes from json-files.

"""

from xsa.errors import *

DIR = 'datatypes/'
"""Directory, where all files are saved"""


def get_all(force=False):
    """
    Reads all json-files in the given directory "DIR". Ignores corrupted files.
    Data maybe retreived from a cache.

    **Parameters**
        :force: bool, if True, the cache is ignored and renewed.
    """
    if get_all.cache and not force:
        return get_all.cache

    from os import listdir
    from os.path import isfile, join
    import json
    get_all.cache = []
    files = [ f for f in listdir(DIR) if isfile(join(DIR,f)) ]  #list of all files in the directory 'DIR'
    for file in files:
        with open(join(DIR,file)) as f:
            try:
                data=json.loads(f.read())
                get_all.cache.append(data)
            except ValueError:
                pass    # if a file can't be read, do nothing
    return get_all()
get_all.cache = None

def get_extra_source(root_type):
    for data in get_all():
        if data['root-type'] == root_type:
            if  not 'extra-source' in data:
                raise NoExtraSourceError("No Source of additional Data was defined for this datatype")
            else:
                return data['extra-source']


def get_rest(root_type):
    """Returns REST-API-Interface of a given Root-Type, defined in its json-file.
    If no REST-API-Interface was defined it will raise a xsa.errors.NoRestApiError.

    **Parameters**
        :root_type: str, Root-Type of returned REST-API-Definition
    """
    for data in get_all():
        if data['root-type'] == root_type:
            if not 'REST-API' in data or not data['REST-API']:
                raise NoRestApiError("No REST-Inteface was defined for this datatype")
            else:
                return data['REST-API']


def get_root_types():
    """Returns all Root-Types."""
    return list(data['root-type'] for data in get_all())


def get_fields(root_type):
    """Returns all fields (including key, label, field,...) of a given Root-Type.

    **Parameters**
        :root_type: str, Root-Type of returned fields
    """
    for data in get_all():
        if data['root-type'] == root_type:
            return data['fields']


def get_field_list(root_type):
    """Returns a list of fields of a given root_type.

    **Parameters**
        :root_type: str, Root-Type of returned field-labels
    """
    fields = get_fields(root_type)
    if fields is not None:
        return list(f['field'] for f in fields)
    return []


def get_labels(root_type):
    """Returns a list of field-labels of a given root_type.

    **Parameters**
        :root_type: str, Root-Type of returned field-labels
    """
    fields = get_fields(root_type)
    if fields is not None:
        return list(f['label'] for f in fields)
    return []


def get_keys(root_type):
    """Returns a list of field-keys of a given root_type.

    **Parameters**
        :root_type: str, Root-Type of returned field-labels
    """
    fields = get_fields(root_type)
    if fields is not None:
        return list(f['key'] for f in fields)
    return []


def get_field_label_by_key(root_type, field_key):
    """Returns the label of the given field-key.

    **Parameters**
        :root_type: str, Root-Type of given key
        :field_key: str, field-Key, for which the label will be returned
    """
    fields = get_fields(root_type)
    for f in fields:
        if "key" in f and f["key"] == field_key:
            return f["label"]
    return field_key


def get_field_by_key(root_type, field_key):
    """Returns the field of the given field-key.

    **Parameters**
        :root_type: str, Root-Type of given key
        :field_key: str, field-Key, for which the label will be returned
    """
    fields = get_fields(root_type)
    for f in fields:
        if "key" in f and f["key"] == field_key:
            return f["field"]
    return field_key


def get_field_key_by_label(root_type, label):
    """Returns the key of the given field-label.

    **Parameters**
        :root_type: str, Root-Type of label
        :label: str, field-label, for which the key will be returned
    """
    fields = get_fields(root_type)
    for f in fields:
        if f['label'] == label:
            return f["key"]
    return label


def get_field_label_by_field(root_type, field):
    """
    Returns the label of the given field.

    **Parameters**
        :root_type: str, Root-Type of given field
        :field: str, field for which the label will be returned
    """
    fields = get_fields(root_type)
    for f in fields:
        if f['field'] == field:
            return f['label']
    return field

def get_field_by_label(root_type, label):
    """
    Returns the field of the given label.

    **Parameters**
        :root_type: str, Root-Type of given field
        :label: str, label for which the field will be returned
    """
    fields = get_fields(root_type)
    for f in fields:
        if f['label'] == label:
            return f['field']
    return label

def get_fields_required(root_type):
    """Returns a list, containing a tuple in which the first value is the label of a field and the second the boolean, if its required or not.

    **Parameters**
        :root_type: Root-Type of fields
    """
    fields = get_fields(root_type)
    if fields is not None:
        return list((f['label'],f['required']) for f in fields)
    return []


def get_fields_from_labels(labels, root_type):
    """Returns a list, including one field for each label.

    **Parameters**
        :labels: list of str, list of labels
        :root_type: str, Root-Type of fields
    """
    result=[]
    fields = get_fields(root_type)
    if fields is not None:
        for l in labels:
            for f in fields:
                if f['label'] == l:
                    result.append(f['field'])
                    break   # if there is more than one label with this name, return just the first one
    return result
