"""

:Author: Franziska Koehn
:Created: 2015/01/13

This module houses functions relating to the chart-creation.

"""

def count_substrings(substrings, strings):
    """
    Returns a dictionary including the counts of its keys.
    This keys are the given list of substrings.

    **Parameters**
        :substrings: list of str, list of substrings to be counted
        :strings:    list of str, strings in which the counts of substrings will be determined
    """
    from collections import defaultdict
    from itertools import product

    if strings == []:
        return []

    subs = list(set(substrings))

    results = defaultdict(int)
    for sub,string in product(subs, strings):
        if sub == '':
            continue
        results[sub] # create key
        if sub.lower() in string.lower():
            results[sub] += 1
    return list(results.items())


def create(data, y_max, x_max, fig):
    """
    creates diagram for given 'data' and max-values ('y_max', 'x_max') and
    renders it as an image, saved as 'file' in given 'size'

    **Parameters**
        :data: dictionary, the key will be shown on the x-axis. the value on the y-axis. if a two-dimensional list will be pass, the values on the first dimension will drawn on the x-axis, the second on the y-axis
        :y_max: max-value for y-axis
        :x_max: max-value for x-axis (= count of keys)
    """

    from matplotlib.figure import Figure

    GNOME_BLUE = '#3A81CC'  # the proper gtk-widgets-colour
    DPI = float(90)         # dots per inch
    BAR_WIDTH = 0.8         # width of bars
    BAR_START = 0.1         # start of bars

    labels = []
    values = []
    for lab, val in data:
        labels.append(lab)
        values.append(val)

    label_pos = []
    x_pos = []
    for x in range(len(labels)):
        label_pos.append(x+BAR_START+BAR_WIDTH/2)
        x_pos.append(x+BAR_START)

    if fig is None:
        fig=Figure()
    fig.clear()

    ax = fig.add_subplot(111)   #111: create a 1 x 1 grid, put the subplot in the 1st cell

    if y_max != 0:
        ax.set_xlim(0, x_max)
        ax.set_ylim(0, y_max)
    else:
        ax.get_yaxis().set_visible(False)

    bars = ax.bar(left=x_pos, height=values, width=BAR_WIDTH, color=GNOME_BLUE)

    ax.get_xaxis().set_ticks(label_pos)
    ax.get_xaxis().set_ticklabels(labels)
    ax.get_yaxis().set_label_text('Count')

    for b in bars:
        height = b.get_height()
        ax.text(b.get_x()+b.get_width()/2., height, '%d'%int(height),
                ha='center', va='bottom', color='black')

    ax.plot()
    return fig
