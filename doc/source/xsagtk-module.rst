
xsagtk
______

In this module are all GUI-related classes and functions. The basis for this is the pygtk libary (version 2.24.0)


Module: :mod:`xsagtk.xsa_app_main`
----------------------------------

.. automodule:: xsagtk.xsa_app_main
  :no-members:

:class: `XsaApp`

.. autoclass:: xsagtk.xsa_app_main.XsaApp
  :members:
  :undoc-members:
  :show-inheritance:
  :private-members:
  :special-members:
  :exclude-members: __module__, __dict__

:method: `start_xsa_gui`

.. automethod:: xsagtk.xsa_app_main.start_xsa_gui



Module: :mod:`xsagtk.main_controller`
-------------------------------------

.. automodule:: xsagtk.main_controller
  :no-members:

:class: `QueryController`

.. autoclass:: xsagtk.main_controller.QueryController
  :members:
  :undoc-members:
  :show-inheritance:
  :special-members:
  :exclude-members: __module__, __dict__


Module: :mod:`xsagtk.menu_view`
-------------------------------

.. automodule:: xsagtk.menu_view
  :no-members:

:class: `MenuView`

.. autoclass:: xsagtk.menu_view.MenuView
  :members:
  :undoc-members:
  :show-inheritance:
  :private-members:
  :special-members:
  :exclude-members: __module__, __dict__


Module: :mod:`xsagtk.query_view`
--------------------------------

.. automodule:: xsagtk.query_view
  :no-members:

:class: `QueryView`

.. autoclass:: xsagtk.query_view.QueryView
  :members:
  :undoc-members:
  :show-inheritance:
  :private-members:
  :special-members:
  :exclude-members: __module__, __dict__


:class: `TreeViewResultFields`

.. autoclass:: xsagtk.query_view.TreeViewResultFields
  :members:
  :undoc-members:
  :show-inheritance:
  :private-members:
  :special-members:
  :exclude-members: __module__, __dict__


:class: `TreeViewQuery`

.. autoclass:: xsagtk.query_view.TreeViewQuery
  :members:
  :undoc-members:
  :show-inheritance:
  :private-members:
  :special-members:
  :exclude-members: __module__, __dict__


Module: :mod:`xsagtk.results_view`
----------------------------------

.. automodule:: xsagtk.results_view
  :no-members:

:class: `ResultsDownloadView`

.. autoclass:: xsagtk.results_view.ResultsDownloadView
  :members:
  :undoc-members:
  :show-inheritance:
  :private-members:
  :special-members:
  :exclude-members: __module__, __dict__

:class: `TreeViewResults`

.. autoclass:: xsagtk.results_view.TreeViewResults
  :members:
  :undoc-members:
  :show-inheritance:
  :private-members:
  :special-members:
  :exclude-members: __module__, __dict__



Module: :mod:`xsagtk.chart_view`
--------------------------------
.. automodule:: xsagtk.chart_view
  :no-members:


:class: `ChartView`

.. autoclass:: xsagtk.chart_view.ChartView
  :members:
  :undoc-members:
  :show-inheritance:
  :private-members:
  :special-members:
  :exclude-members: __module__, __dict__


:class: `ComboBoxRootType`

.. autoclass:: xsagtk.chart_view.ComboBoxRootType
  :members:
  :undoc-members:
  :show-inheritance:
  :private-members:
  :special-members:
  :exclude-members: __module__, __dict__


:class: `TreeViewChartValues`

.. autoclass:: xsagtk.chart_view.TreeViewChartValues
  :members:
  :undoc-members:
  :show-inheritance:
  :private-members:
  :special-members:
  :exclude-members: __module__, __dict__
