#!/usr/bin/env python
from __future__ import print_function

import argparse
import getpass
import sys
import requests
import json
import xsa.errors
import xsa.queries as queries
import xsa.datatypereader as datatypereader

from requests.auth import HTTPBasicAuth

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='search and download')
    parser.add_argument('host', type=str, help='hostname or ip-address (including port).', default="localhost:8080")
    parser.add_argument('user', type=str, help='username')
    parser.add_argument('query', type=argparse.FileType('r'), default=sys.stdin, nargs="?", help='query-file includes root-type, query, and requested fieldnames (stdin if left out)')
    parser.add_argument('--csv_out', '-o', metavar="<filename>", type=str, default=None,
                        help='name of the result-csv-file. If not set, no result-file will be generated and the regex will be ignored.')
    parser.add_argument('--passw', '-p', type=str, default=None, help='Password of your user')

    args = parser.parse_args()
    host = args.host.strip('/')
    user = args.user
    query = json.load(args.query)
    csv_out = args.csv_out
    passw = args.passw

    if not passw:
        while True:
            passw = getpass.getpass('Password for %s:' % user)
            if requests.get(host, auth=HTTPBasicAuth(user, passw)).status_code != 200:
                print("credentials are not correct")
            else:
                print("password accepted")
                break


    root_element = query['root']

    def convert(l):
        if isinstance(l, str):
            return l
        elif len(l) == 3 and all(isinstance(e, str) for e in l):
            return tuple(l)
        else:
            return list(convert(e) for e in l)

    constraints = convert(query['query'])

    fields = query.get('fields', None) or query.get('labels')
    search_fields = []
    if fields:
        for field in fields:
            if field in datatypereader.get_labels(root_element):
                search_fields.append(datatypereader.get_field_by_label(root_element, field))
            elif field in datatypereader.get_keys(root_element):
                search_fields.append(datatypereader.get_field_by_key(root_element, field))
            elif field in datatypereader.get_field_list(root_element):
                search_fields.append(field)
            else:
                print('cant use this: %s , removed it from list of requested fields!', field)
    else:
        raise xsa.errors.QueryError('No search-fields are defined in the passed query (use <fields> or <labels> as key)!')

    results = queries.search_for(host, root_element, constraints, search_fields, user, passw)

    print(results)
    if csv_out:
        results.dump_csv(csv_out, delimiter="|")
        print("saved to %s" % csv_out)
