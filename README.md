# Readme

I recomment to checkout releases
```git clone -b RELEASENAME git@mosel.cbs.mpg.de:xnat/xnat-scan-ambassador.git```

If you have already checked out XSA, run following command to update your repository
```git fetch && git checkout RELEASENAME```

###RELEASENAMES

date | release-tag
--- | ---
2015.01.20 | release20150120

## Prerequisites
install with your Package Manager:
* pygtk (mac: http://sourceforge.net/projects/macpkg/files/PyGTK/2.24.0/)

install with pip:
* pyxnat (httplib2, lxml)
* requests
* pydash
* matplotlib

* install by using prerequisites.txt:

```pip install --user -r prerequisites.txt```

## How to use

* start as script
```
python xsa_script.py --help
python xsa_script.py --host https://yourHost --user user:passw
./xsa_script.py --user user:passw --host https://yourHost

```

* start with graphical user interface
```
python xsa_gui.py --user user:passw --host https://yourHost
./xsa_gui.py --user user:passw --host https://yourHost
```
(fills user, password and host text-fields (accessable via the "Server Settings"-Button))
```
python xsa_gui.py
./xsa_gui.py
```
(starts with empty text-fields)
